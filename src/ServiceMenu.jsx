import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import { BadgeSample } from "./components/BadgeSample";
import "./ui/ServiceMenu.css";

class ServiceMenu extends Component {
    constructor(props) {
        super(props);

        this.onClickHandler = this.onClick.bind(this);
    }

    render() {
        return (
            <BadgeSample
                type={this.props.servicemenuType}
                bootstrapStyle={this.props.bootstrapStyle}
                className={this.props.class}
                clickable={!!this.props.onClickAction}
                defaultValue={this.props.servicemenuValue ? this.props.servicemenuValue : ""}
                onClickAction={this.onClickHandler}
                style={this.props.style}
                value={this.props.valueAttribute ? this.props.valueAttribute.displayValue : ""}
            />
        );
    }

    onClick() {
        if (this.props.onClickAction && this.props.onClickAction.canExecute) {
            this.props.onClickAction.execute();
        }
    }
}

export default hot(ServiceMenu);
